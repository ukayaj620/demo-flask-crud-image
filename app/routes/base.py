from flask import Blueprint, render_template, request, redirect, url_for
from flask_login import login_required, current_user
from app.controllers.auth import AuthController
from app.controllers.book import BookController


base = Blueprint('base', __name__, template_folder='templates')
auth_controller = AuthController()
book_controller = BookController()

@base.route('/', methods=['GET'])
def index():
  if current_user.is_authenticated:
    return auth_controller.determine_redirection(role=current_user.role)
  return redirect(url_for('auth.login'))

@base.route('/home', methods=['GET'])
def home():
  books = book_controller.fetch_all()
  return render_template('client/index.html', data={
    'books': books
  })