from flask import Blueprint, request, redirect, render_template, url_for
from flask_login import login_required
from app.controllers.auth import AuthController


auth = Blueprint('auth', __name__, template_folder='templates')
auth_controller = AuthController()

@auth.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    remember = True if request.form.get('remember') else False
    return auth_controller.login(request=request.form, remember=remember)
  
  return render_template('auth/login.html')


@auth.route('/logout', methods=['GET'])
@login_required
def logout():
  return auth_controller.logout()

