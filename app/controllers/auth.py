from flask import flash, redirect, url_for
from app.models.user import User
from werkzeug.security import check_password_hash
from flask_login import login_user, logout_user
from datetime import timedelta


class AuthController:

  def __init__(self):
    self.user = User()

  def determine_redirection(self, role):
    choice = {
      'user': 'base.home',
      'admin': 'admin.index',
    }

    return redirect(url_for(choice.get(role, 'error.unauthorized')))

  def login(self, request, remember):
    user = self.user.query.filter_by(username=request['username']).first()
    if not user or not check_password_hash(user.password, request['password']):
      flash('Wrong credentials!', 'danger')
      return redirect(url_for('auth.login'))

    login_user(user, remember=remember, duration=timedelta(days=30))

    return self.determine_redirection(role=user.role)

  def logout(self):
    logout_user()
    return redirect(url_for('auth.login'))