# Simple CRUD + Image Demo using Flask

Follow the steps below in order to try it:
- Install all packages listed in ```requirements.txt``` on your local virtual environment.
- Create database using MySQL. The database name is **library**.
- Type ```flask db upgrade``` command on your project terminal.
- Set app.py as FLASK_APP (```set FLASK_APP=app.py```).
- Set environment to development (```set FLASK_ENV=development```).

This simple demonstration is created in order to accomplish my university task.
